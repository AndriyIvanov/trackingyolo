#include <iostream>
#include <string>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include <QElapsedTimer>

#include <dlib/image_processing.h>
#include <dlib/opencv.h>

#include "yolo_v2_class.hpp"


using namespace std;
//using namespace cv;

#define SSTR(x) static_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str()

cv::Point centerPoint(bbox_t r)
{
    return cv::Point(int(r.x+r.w/2), int(r.y+r.h/2));
}

cv::Rect Dlib_RectToCV(dlib::drectangle r)
{
    return cv::Rect(cv::Point2i(r.left(), r.top()), cv::Point2i(r.right() + 1, r.bottom() + 1));
}

enum TrackedObjects{Person, Bicycle, Car, Motorbike, Aeroplane, Bus, Train, Truck, Boat};

const string cfg_path = "/home/nvidia/Downloads/darknet/cfg/yolov3.cfg";
const string weight_path = "/home/nvidia/Downloads/darknet/yolov3.weights";

//const string cfg_path = "/home/nvidia/Downloads/darknet/cfg/yolov3-tiny.cfg";
//const string weight_path = "/home/nvidia/Downloads/darknet/yolov3-tiny.weights";


const string pathToVideo ("/home/nvidia/Projects/Dlib_KCF_Tracker/Dlib_KCF_Tracker/Data/Run 100 m.mp4");

double TRESHOLD = 0.8;                                //Probability threshold for Yolo detection
const TrackedObjects objType = Person;

std::vector<bbox_t> resultsYolo;

int main()
{
    Detector* YoloDetector = new Detector(cfg_path, weight_path);

    //cv::VideoCapture video(1);
    cv::VideoCapture video(pathToVideo);
    video.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    video.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

    if(!video.isOpened())
    {
        cout << "Could not read video file" << endl;
        return EXIT_FAILURE;
    }

    cv::Mat frame;

    QElapsedTimer timer;

    bool trackInit = true;
    dlib::array2d<dlib::bgr_pixel> dlibFrame;
    dlib::correlation_tracker dlibTracker;
    dlib::drectangle out_box_dlib;
    double confidence;

    while(true)
    {
        // Start timer

        //auto start = chrono::steady_clock::now();
        timer.restart();
        video.read(frame);
        dlib::assign_image(dlibFrame, dlib::cv_image<dlib::bgr_pixel>(frame));

        string frame_dim = "frame dimensions (WxH): " + to_string(frame.rows) + "x" + to_string(frame.cols);

        if (trackInit)
        {
            resultsYolo = YoloDetector->detect(frame, TRESHOLD);

            if(resultsYolo.size() > 0)  //If nothing found by Yolo
            {
                for (auto &r : resultsYolo)
                {
                     if(r.prob > TRESHOLD && r.obj_id == objType)
                     {
                         cv::rectangle(frame, cv::Rect(r.x, r.y, r.w, r.h), cv::Scalar(0,255,0), 2, 1);
                         if(trackInit)
                         {
                             dlibTracker.start_track(dlibFrame, dlib::drectangle(r.x, r.y, r.x + r.w, r.y + r.h));
                             cv::rectangle(frame, cv::Rect(r.x, r.y, r.w, r.h), cv::Scalar(0,0,255), 3, 1);
                             trackInit = false;
                         }
                     }
                }
            }
            cv::imshow("Yolo", frame);
            if(cv::waitKey(1) == 27) break;
            continue;
        }
        confidence = dlibTracker.update(dlibFrame);
        if (confidence > 10)
        {
            out_box_dlib = dlibTracker.get_position();
            cv::rectangle(frame, Dlib_RectToCV(out_box_dlib), cv::Scalar(0,0,255), 2, 1);
        }
        else
        {
            cv::Rect dlibRect = Dlib_RectToCV(out_box_dlib);

            resultsYolo = YoloDetector->detect(frame, TRESHOLD);

            if(resultsYolo.size() > 0)  //If nothing found by Yolo
            {
                for (auto &r : resultsYolo)
                {
                     if(r.prob > TRESHOLD && r.obj_id == objType)
                     {
                         if (dlibRect.contains(centerPoint(r)))
                         {
                             dlibTracker.start_track(dlibFrame, dlib::drectangle(r.x, r.y, r.x + r.w, r.y + r.h));
                         }
                         break;
                     }
                }
            }
        }


        // Calculate Frames per second (FPS)
        //double time = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        int timer_elapsed = timer.elapsed();


        // Display FPS on frame
        cv::putText(frame, frame_dim, cv::Point(10,25), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,255,0), 2);
        cv::putText(frame, "Calc. time : " + SSTR(timer_elapsed), cv::Point(10,50), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,255,0), 2);

        // Display frame.
        cv::imshow("Yolo", frame);

        // Exit if ESC pressed.
        if(cv::waitKey(1) == 27) break;
    }


    return 0;

}

