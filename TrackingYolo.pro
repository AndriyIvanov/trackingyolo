QT += core
QT -= gui

TARGET = TrackingYolo
CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig

QMAKE_CXXFLAGS += -std=c++14
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH +='/usr/include/aarch64-linux-gnu/qt5/'

PKGCONFIG += opencv
PKGCONFIG += dlib-1

INCLUDEPATH += /home/nvidia/Downloads/darknet/src/
INCLUDEPATH += /home/nvidia/Downloads/darknet/include/
LIBS += -L'/home/nvidia/Downloads/darknet' -ldarknet

TEMPLATE = app

SOURCES += main.cpp

